#!/bin/bash
# Creates some projects and 15 tasks on a test Phabricator instance.
# Used for a tutorial Video about Wikimedia's Phabricator instance.
# User accounts must be created manually beforehand.
# See https://phabricator.wikimedia.org/T214522
#
# This code is licensed under CC0 1.0 Universal: 
# https://creativecommons.org/publicdomain/zero/1.0/legalcode
# Author: Andre Klapper
# Date: 2020-08-18

PHABURL="enter url here"
CONDUITTOKEN="enter token here"

echo "================================="
echo "WARNING: This script WILL fail for creating milestones. See:"
echo "https://discourse.phabricator-community.org/t/call-to-a-member-function-getphid-on-a-non-object-while-creating-milestone-using-conduit/3370/5"
echo "Before running this script, you must either backport:"
echo "https://github.com/grembo/phabricator/commit/0851b89eb6633dd792cd4eb10c26f86c2f0da56a.diff"
echo "See https://secure.phabricator.com/T13553 for a future proper fix."
echo "Cancel this script via Ctrl+C if you have not fixed this yet!"
echo "================================="
sleep 30;

# Get PHIDs for previously manually created users:
USERPHID1=( $(echo '{"constraints":{"usernames":["'Maria'"]}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN user.search -- | jq '.response.data[0].phid'))
USERPHID2=( $(echo '{"constraints":{"usernames":["'Yves'"]}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN user.search -- | jq '.response.data[0].phid'))
USERPHID3=( $(echo '{"constraints":{"usernames":["'Kate'"]}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN user.search -- | jq '.response.data[0].phid'))
USERPHID4=( $(echo '{"constraints":{"usernames":["'Finley'"]}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN user.search -- | jq '.response.data[0].phid'))
USERPHID5=( $(echo '{"constraints":{"usernames":["'Cassidy'"]}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN user.search -- | jq '.response.data[0].phid'))

# Create seven parent projects: 
## Security
echo '{"transactions":[{"type":"name","value":"XXX--Security"},{"type":"icon", "value":"tag"},{"type":"color","value":"yellow"},{"type":"description","value":"Security stuff"}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.edit --
## Patch-For-Review
echo '{"transactions":[{"type":"name","value":"XXX--Patch-For-Review"},{"type":"icon", "value":"meta"},{"type":"color","value":"pink"},{"type":"description","value":"Security stuff"}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.edit --
## Garden
echo '{"transactions":[{"type":"name","value":"XXX--Garden"},{"type":"icon", "value":"component"},{"type":"color","value":"blue"},{"type":"description","value":"Garden around the #House - see https://en.wikipedia.org/wiki/Garden for general information."},{"type":"members.add","value":['$USERPHID1','$USERPHID2']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.edit --
## House
echo '{"transactions":[{"type":"name","value":"XXX--House"},{"type":"icon", "value":"component"},{"type":"color","value":"blue"},{"type":"description","value":"A building which is warm in winter and dry when it rains outside."}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.edit --
## Electricians
echo '{"transactions":[{"type":"name","value":"XXX--Electricians"},{"type":"icon", "value":"group"},{"type":"color","value":"violet"},{"type":"description","value":"Team which brings electrons safely from one place to another"},{"type":"members.add","value":['$USERPHID5','$USERPHID3']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.edit --
## Plumbers
echo '{"transactions":[{"type":"name","value":"XXX--Plumbers"},{"type":"icon", "value":"group"},{"type":"color","value":"violet"},{"type":"description","value":"Team which fixes anything related to clean or dirty water."},{"type":"members.add","value":['$USERPHID4']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.edit --
## Craftspeople
echo '{"transactions":[{"type":"name","value":"XXX--Craftspeople"},{"type":"icon", "value":"group"},{"type":"color","value":"violet"},{"type":"description","value":"Team which fixes anything related to craftswork."},{"type":"members.add","value":['$USERPHID3', '$USERPHID2']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.edit --
## ACME
echo '{"transactions":[{"type":"name","value":"XXX--ACME"},{"type":"icon", "value":"component"},{"type":"color","value":"blue"},{"type":"description","value":"This tag helps with the discovery of issues related to the currently ongoing project to build ACME. ACME is the new search engine which will allow to find things in the house in the garden."}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.edit --

# Create two subprojects under Garden:
PROJECTPHIDgarden=( $(echo '{"constraints":{"slugs":["XXX--Garden"]}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.search -- | jq '.response.data[0].phid'))
## Tennis court
echo '{"transactions":[{"type":"parent", "value":'$PROJECTPHIDgarden'},{"type":"name","value":"XXX--Tennis-Court"},{"type":"icon", "value":"component"},{"type":"color","value":"blue"},{"type":"description","value":"Sports venue for two or four people."}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.edit --
## Food plants
echo '{"transactions":[{"type":"parent", "value":'$PROJECTPHIDgarden'},{"type":"name","value":"XXX--Food-Plants"},{"type":"icon", "value":"component"},{"type":"color","value":"blue"},{"type":"description","value":"Place to grow some food."}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.edit --

# Create two milestones within Food-Plants subproject:
PROJECTPHIDfoodplants=( $(echo '{"constraints":{"slugs":["XXX--Food-Plants"]}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.search -- | jq '.response.data[0].phid'))
## Year 2026
echo '{"transactions":[{"type":"milestone", "value":'$PROJECTPHIDfoodplants'},{"type":"name","value":"XXX--2026"},{"type":"icon", "value":"component"},{"type":"color","value":"blue"},{"type":"description","value":"Plans for the Garden in 2026."}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.edit --
## Year 2027
echo '{"transactions":[{"type":"milestone", "value":'$PROJECTPHIDfoodplants'},{"type":"name","value":"XXX--2027"},{"type":"icon", "value":"component"},{"type":"color","value":"blue"},{"type":"description","value":"Plans for the Garden in 2027."}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.edit --

# Create tasks:

PROJECTPHIDhouse=( $(echo '{"constraints":{"slugs":["XXX--House"]}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.search -- | jq '.response.data[0].phid'))

PROJECTPHIDplumbers=( $(echo '{"constraints":{"slugs":["XXX--Plumbers"]}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.search -- | jq '.response.data[0].phid'))

PROJECTPHIDtenniscourt=( $(echo '{"constraints":{"slugs":["XXX--Tennis-Court"]}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.search -- | jq '.response.data[0].phid'))

PROJECTPHIDelectricians=( $(echo '{"constraints":{"slugs":["XXX--Electricians"]}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.search -- | jq '.response.data[0].phid'))

PROJECTPHIDcraftspeople=( $(echo '{"constraints":{"slugs":["XXX--Craftspeople"]}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.search -- | jq '.response.data[0].phid'))

PROJECTPHIDsecurity=( $(echo '{"constraints":{"slugs":["XXX--Security"]}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.search -- | jq '.response.data[0].phid'))

# Do not use slugs here but name. Slugs will be empty for milestones:
PROJECTPHIDfoodplants2026=( $(echo '{"constraints":{"name":"XXX--2026"}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.search -- | jq '.response.data[0].phid'))

# Do not use slugs here but name. Slugs will be empty for milestones:
PROJECTPHIDfoodplants2027=( $(echo '{"constraints":{"name":"XXX--2027"}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN project.search -- | jq '.response.data[0].phid'))


echo '{"transactions": [{"type": "title", "value":"Put a name on the mailbox"}, {"type": "description", "value":"your task desc"}, {"type": "status", "value":"resolved"}, {"type": "subscribers.add", "value":['$USERPHID3','$USERPHID4']}, {"type": "owner", "value":'$USERPHID4'}, {"type": "view", "value":"public"}, {"type": "priority", "value":"high"}, {"type":"projects.add", "value":['$PROJECTPHIDhouse']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

echo '{"transactions": [{"type": "title", "value":"Take a bath"}, {"type": "description", "value":"Stink less!"}, {"type": "status", "value":"stalled"}, {"type": "subscribers.add", "value":['$USERPHID4']}, {"type": "owner", "value":'$USERPHID3'}, {"type": "view", "value":"public"}, {"type": "priority", "value":"triage"}, {"type":"projects.add", "value":['$PROJECTPHIDhouse']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

echo '{"transactions": [{"type": "title", "value":"Repair leaking pipe in bathroom"}, {"type": "description", "value":"**Steps to reproduce:**\n\n1. Open the door to the bathroom\n2. Enter the bathroom\n\n**Expected results:**\n\nA dry floor\n\n**Actual results:**\n\nThe floor is wet"}, {"type": "status", "value":"open"}, {"type": "subscribers.add", "value":['$USERPHID1','$USERPHID4']}, {"type": "owner", "value":'$USERPHID5'}, {"type": "view", "value":"public"}, {"type": "priority", "value":"high"}, {"type":"projects.add", "value":['$PROJECTPHIDhouse','$PROJECTPHIDplumbers']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

echo '{"transactions": [{"type": "title", "value":"Paint the entire house in orange and violet colors"}, {"type": "description", "value":"your task desc"}, {"type": "status", "value":"declined"}, {"type": "subscribers.add", "value":['$USERPHID1','$USERPHID4']}, {"type": "owner", "value":null}, {"type": "view", "value":"public"}, {"type": "priority", "value":"lowest"}, {"type":"projects.add", "value":['$PROJECTPHIDhouse', '$PROJECTPHIDcraftspeople']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

echo '{"transactions": [{"type": "title", "value":"Two instead of one front door keyhole when coming home a bit drunk"}, {"type": "description", "value":"**Steps to reproduce:**\n\n1. Leave the house with your keys.\n2. Consume some alcohol.\n3. Go back to the house.\n4. Try to open the door with the key.\n\n**Expected results:**\n\nOnly one keyhole (like when I left)\n\n**Actual results:**\n\nTwo keyholes; unsure which one to use"}, {"type": "status", "value":"invalid"}, {"type": "owner", "value":null}, {"type": "view", "value":"public"}, {"type": "priority", "value":"triage"}, {"type":"projects.add", "value":['$PROJECTPHIDhouse']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

echo '{"transactions": [{"type": "title", "value":"Deploy a grumpy dog at the entrance"}, {"type": "description", "value":"Feature request. Use case: Creating an illusory sense of security."}, {"type": "status", "value":"open"}, {"type": "subscribers.add", "value":['$USERPHID5','$USERPHID4']}, {"type": "owner", "value":null}, {"type": "view", "value":"public"}, {"type": "priority", "value":"low"}, {"type":"projects.add", "value":['$PROJECTPHIDgarden']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

echo '{"transactions": [{"type": "title", "value":"Set up a pond or swimming pool"}, {"type": "description", "value":"Water!"}, {"type": "status", "value":"declined"}, {"type": "subscribers.add", "value":['$USERPHID2']}, {"type": "owner", "value":null}, {"type": "view", "value":"public"}, {"type": "priority", "value":"low"}, {"type":"projects.add", "value":['$PROJECTPHIDgarden']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

echo '{"transactions": [{"type": "title", "value":"Pave the path from the house to the street"}, {"type": "description", "value":"It sometimes rains and my shoes prefer this."}, {"type": "status", "value":"resolved"}, {"type": "subscribers.add", "value":['$USERPHID1']}, {"type": "owner", "value":'$USERPHID5'}, {"type": "view", "value":"public"}, {"type": "priority", "value":"normal"}, {"type":"projects.add", "value":['$PROJECTPHIDgarden', '$PROJECTPHIDcraftspeople']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

echo '{"transactions": [{"type": "title", "value":"Repair the broken fence"}, {"type": "description", "value":"Might require some new wood."}, {"type": "status", "value":"open"}, {"type": "subscribers.add", "value":['$USERPHID2','$USERPHID3']}, {"type": "owner", "value":'$USERPHID4'}, {"type": "view", "value":"public"}, {"type": "priority", "value":"high"}, {"type":"projects.add", "value":['$PROJECTPHIDgarden', '$PROJECTPHIDcraftspeople']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

echo '{"transactions": [{"type": "title", "value":"Door from garden to street cannot be locked"}, {"type": "description", "value":"There is currently no way to lock the door from the garden to the street - there simply is no locker next to the knob."}, {"type": "status", "value":"open"}, {"type": "subscribers.add", "value":['$USERPHID3','$USERPHID4']}, {"type": "owner", "value":'$USERPHID4'}, {"type": "view", "value":"public"}, {"type": "priority", "value":"high"}, {"type":"projects.add", "value":['$PROJECTPHIDgarden','$PROJECTPHIDsecurity', '$PROJECTPHIDcraftspeople']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

echo '{"transactions": [{"type": "title", "value":"Get five beanpoles"}, {"type": "description", "value":"Get five long sticks for growing beans."}, {"type": "status", "value":"open"}, {"type": "subscribers.add", "value":['$USERPHID2']}, {"type": "owner", "value":null}, {"type": "view", "value":"public"}, {"type": "priority", "value":"high"}, {"type":"projects.add", "value":['$PROJECTPHIDfoodplants2026']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

echo '{"transactions": [{"type": "title", "value":"Dig holes in the ground for beanpoles"}, {"type": "description", "value":"Dig five holes in the ground for growing beans."}, {"type": "status", "value":"open"}, {"type": "subscribers.add", "value":['$USERPHID4']}, {"type": "owner", "value":null}, {"type": "view", "value":"public"}, {"type": "priority", "value":"normal"}, {"type":"projects.add", "value":['$PROJECTPHIDfoodplants2026']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

echo '{"transactions": [{"type": "title", "value":"Set up beanpoles and plant green beans in the garden"}, {"type": "description", "value":"Dig holes into the ground and then put the long sticks for growing beans in there."}, {"type": "status", "value":"open"}, {"type": "subscribers.add", "value":['$USERPHID2','$USERPHID3']}, {"type": "owner", "value":null}, {"type": "view", "value":"public"}, {"type": "priority", "value":"low"}, {"type":"projects.add", "value":['$PROJECTPHIDfoodplants2026']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

echo '{"transactions": [{"type": "title", "value":"Set up apple tree"}, {"type": "description", "value":"[ ] Decide on kind of apple\n[ ] Decide on exact spot for apple tree\n[ ] Get apple tree\n[ ] Make a hole in the ground\n[ ] Plant it\n[ ] Initially water it"}, {"type": "status", "value":"open"}, {"type": "subscribers.add", "value":['$USERPHID2','$USERPHID1']}, {"type": "owner", "value":null}, {"type": "view", "value":"public"}, {"type": "priority", "value":"triage"}, {"type":"projects.add", "value":['$PROJECTPHIDfoodplants2027']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

echo '{"transactions": [{"type": "title", "value":"Grow potatoes"}, {"type": "description", "value":"Set up area in the garden to grow some potatoes."}, {"type": "status", "value":"open"}, {"type": "subscribers.add", "value":['$USERPHID4','$USERPHID3']}, {"type": "points", "value":"5"}, {"type": "owner", "value":null}, {"type": "view", "value":"public"}, {"type": "priority", "value":"low"}, {"type":"projects.add", "value":['$PROJECTPHIDfoodplants']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --


#echo '{"transactions": [{"type": "title", "value":"Get second racket"}, {"type": "description", "value":"In order to play tennis with more than one person, a second racket is needed."}, {"type": "status", "value":"open"}, {"type": "subscribers.add", "value":['$USERPHID2','$USERPHID3']}, {"type": "points", "value":"5"}, {"type": "owner", "value":'$USERPHID4'}, {"type": "view", "value":"public"}, {"type": "priority", "value":"high"}, {"type":"projects.add", "value":['$PROJECTPHIDtenniscourt']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

#echo '{"transactions": [{"type": "title", "value":"Fix holes in tennis net"}, {"type": "description", "value":"IT IS BROKEN!!!!!."}, {"type": "status", "value":"open"}, {"type": "subscribers.add", "value":['$USERPHID2','$USERPHID1']}, {"type": "points", "value":"2"}, {"type": "view", "value":"public"}, {"type": "priority", "value":"normal"}, {"type":"projects.add", "value":['$PROJECTPHIDtenniscourt']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

#echo '{"transactions": [{"type": "title", "value":"Install digital scoreboard"}, {"type": "description", "value":"Shiny shiny!"}, {"type": "status", "value":"open"}, {"type": "subscribers.add", "value":['$USERPHID2','$USERPHID3']}, {"type": "owner", "value":'$USERPHID4'}, {"type": "view", "value":"public"}, {"type": "priority", "value":"lowest"}, {"type":"projects.add", "value":['$PROJECTPHIDtenniscourt','$PROJECTPHIDelectricians']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

#echo '{"transactions": [{"type": "title", "value":"Investigate installing hawkeye technology"}, {"type": "description", "value":"**Steps to reproduce:**\n\n1. Play tennis.\n2. Drop a ball very close to a white line.\n\n**Expected results:**\n\nTechnology to tell me if it the tennis ball still hit the line or if the ball was out\n\n**Actual results:**\n\nCurrently there is endless quarrel and fights"}, {"type": "status", "value":"open"}, {"type": "subscribers.add", "value":['$USERPHID3']}, {"type": "points", "value":"8"}, {"type": "owner", "value":'$USERPHID1'}, {"type": "view", "value":"public"}, {"type": "priority", "value":"low"}, {"type":"projects.add", "value":['$PROJECTPHIDtenniscourt','$PROJECTPHIDelectricians']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --

#echo '{"transactions": [{"type": "title", "value":"Set up lights to be able to play at night"}, {"type": "description", "value":"Even more shiny shiny!"}, {"type": "status", "value":"open"}, {"type": "subscribers.add", "value":['$USERPHID5','$USERPHID3']}, {"type": "owner", "value":'$USERPHID4'}, {"type": "view", "value":"public"}, {"type": "priority", "value":"lowest"}, {"type":"projects.add", "value":['$PROJECTPHIDtenniscourt','$PROJECTPHIDelectricians']}]}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit --
